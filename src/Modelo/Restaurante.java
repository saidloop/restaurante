/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Restaurante {
    
    private Carta carta;
    private Mesa mesas[];

    public Restaurante() {
    }

    public Restaurante(Carta carta, Mesa[] mesas) {
        this.carta = carta;
        this.mesas = mesas;
    }

    public Carta getCarta() {
        return carta;
    }

    public Mesa[] getMesas() {
        return mesas;
    }

    public void setCarta(Carta carta) {
        this.carta = carta;
    }

    public void ApartarMesa(String id, Cliente cliente) {
        for(int i=0; i<mesas.length; i++){
           if(mesas[i].getId()==id && mesas[i].isApartada()==false){
               mesas[i].setApartada(true);
               mesas[i].setCliente(cliente);
               i=mesas.length;
           } else {
               System.out.println("No es posible, mesa ya ha sido reservada");
           }
        }
    }
    
    
}
